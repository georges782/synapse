﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Synapse.Metiers
{
    [Serializable()]
   public class Intervenant
    {
        private string _nom;
        private decimal _tauxHoraire;

        public decimal TauxHoraire
        {
            get
            {
                return _tauxHoraire;
            }
        }

        public string Nom
        {
            get
            {
                return _nom;
            }
        } 

        public Intervenant (string nom, decimal tauxHoraire)
        {
            _nom = nom;
            _tauxHoraire = tauxHoraire;
        }

        public override bool Equals (object unIntervenant)
        {
            bool memeInfoIntervenant = false;

            Intervenant i = unIntervenant as Intervenant;

            if (i != null)
            {
                if (_nom == i._nom && _tauxHoraire == i._tauxHoraire)
                    memeInfoIntervenant = true;
            }
            return memeInfoIntervenant;
        }
    }

}
