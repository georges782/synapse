﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Synapse.Metiers
{
    [Serializable()]
   public class Projet
    {
        private decimal _prixFactureMO;
        private DateTime _debut;
        private DateTime _fin;
        private decimal _cumulCoutMO;
        List<Mission> _missions; 

        private decimal CumulCoutMO()
        {
            decimal resultat = 0;

            foreach (Mission missionCourante in _missions)
            {
                resultat = resultat + (missionCourante.NbHeuresEffectuees() * missionCourante.Executant.TauxHoraire);
            }
            return resultat;
        }

        public object MargeBruteCourante()
        {
            decimal resultat = 0;
            resultat = _prixFactureMO - CumulCoutMO();

            return resultat;
        }
    }
}
