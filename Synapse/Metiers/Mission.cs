﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Synapse.Metiers
{
    [Serializable()]
    class Mission
    {
        private int _nbHeuresPreuves;
        private string _nom;
        private string _description;
        private Dictionary<DateTime, int> _releveHoraire;

        private Intervenant _executant;

        public Dictionary<DateTime, int> ReleveHoraire
        {
            get
            {
                return _releveHoraire; 
            }
        }

        public Intervenant Executant
        {
            get
            {
                return _executant;
            }

        }

        public void AjouteReleve(DateTime date, int _nbHeuresPreuves)
        {
            ReleveHoraire.Add(date, _nbHeuresPreuves);
        }
        public int NbHeuresEffectuees()
        {
            int resultat = 0;

            foreach (KeyValuePair<DateTime,  int> ReleveHoraire in _releveHoraire)
            {
                resultat = ReleveHoraire.Value;
         
            }
            return resultat;
        }

    }
}
