﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Runtime.Serialization;

namespace Synapse.Utilitaires
{
    public static class Persistances
    {
        private static string repertoireApplication = Environment.CurrentDirectory + @"\";

        public static IList ChargerDonnees(string nomFichier)
        {
            FileStream fs = null;
            IList listeItem = null;

            try
            {
                fs = new FileStream(repertoireApplication + nomFichier, FileMode.Open);
                BinaryFormatter formatter = new BinaryFormatter();
                try
                {
                    listeItem = (IList)formatter.Deserialize(fs);
                }
                catch (SerializationException err)
                {
                    Persistances.FichierLog(err);
                }
                finally
                {
                    fs.Close();
                }
            }
            catch (Exception erreur)
            {

            }
            return listeItem;
        }
    }
}

