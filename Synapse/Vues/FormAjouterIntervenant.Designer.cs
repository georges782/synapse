﻿namespace Synapse
{
    partial class Form1
    {
        /// <summary>
        /// Variable nécessaire au concepteur.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Nettoyage des ressources utilisées.
        /// </summary>
        /// <param name="disposing">true si les ressources managées doivent être supprimées ; sinon, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Code généré par le Concepteur Windows Form

        /// <summary>
        /// Méthode requise pour la prise en charge du concepteur - ne modifiez pas
        /// le contenu de cette méthode avec l'éditeur de code.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.Nom = new System.Windows.Forms.TextBox();
            this.NomIt = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.ButtonValider = new System.Windows.Forms.Button();
            this.TauxHoraire = new System.Windows.Forms.TextBox();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.TauxHoraire);
            this.groupBox1.Controls.Add(this.ButtonValider);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.NomIt);
            this.groupBox1.Controls.Add(this.Nom);
            this.groupBox1.Location = new System.Drawing.Point(12, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(385, 238);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Intervenant";
            this.groupBox1.Enter += new System.EventHandler(this.groupBox1_Enter);
            // 
            // Nom
            // 
            this.Nom.Location = new System.Drawing.Point(149, 26);
            this.Nom.Name = "Nom";
            this.Nom.Size = new System.Drawing.Size(100, 20);
            this.Nom.TabIndex = 1;
            this.Nom.TextChanged += new System.EventHandler(this.textBox1_TextChanged);
            // 
            // NomIt
            // 
            this.NomIt.AutoSize = true;
            this.NomIt.Location = new System.Drawing.Point(114, 29);
            this.NomIt.Name = "NomIt";
            this.NomIt.Size = new System.Drawing.Size(29, 13);
            this.NomIt.TabIndex = 2;
            this.NomIt.Text = "Nom";
            this.NomIt.Click += new System.EventHandler(this.label1_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(75, 64);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(68, 13);
            this.label1.TabIndex = 3;
            this.label1.Text = "Taux Horaire";
            // 
            // ButtonValider
            // 
            this.ButtonValider.Location = new System.Drawing.Point(174, 108);
            this.ButtonValider.Name = "ButtonValider";
            this.ButtonValider.Size = new System.Drawing.Size(75, 23);
            this.ButtonValider.TabIndex = 4;
            this.ButtonValider.Text = "Valider ";
            this.ButtonValider.UseVisualStyleBackColor = true;
            // 
            // TauxHoraire
            // 
            this.TauxHoraire.Location = new System.Drawing.Point(149, 64);
            this.TauxHoraire.Name = "TauxHoraire";
            this.TauxHoraire.Size = new System.Drawing.Size(100, 20);
            this.TauxHoraire.TabIndex = 5;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.ClientSize = new System.Drawing.Size(401, 263);
            this.Controls.Add(this.groupBox1);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox Nom;
        private System.Windows.Forms.Label NomIt;
        private System.Windows.Forms.TextBox TauxHoraire;
        private System.Windows.Forms.Button ButtonValider;
        private System.Windows.Forms.Label label1;
    }
}

